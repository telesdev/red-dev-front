import { Route, Router, Switch } from 'react-router-dom'
import { history }from './config/history'

//views
import { MinhaRede, Painel, Perfil, Post, SignUp, SignIn } from './views/index'
import { SignHeader } from './views/sign/header/SignHeader'
import { SignFooter } from './views/sign/footer/SignFooter'
import { Header } from './components/layout/header/Header'
import { Footer } from './components/layout/footer/Footer'

export const Routes = () => {
    return (
    <Router history={history}>
        <Switch>
            <Route exact path='/'>
                <Header />
                Home
                <Footer />
            </Route>
            <Route path='/minharede'>
                <Header />
                <MinhaRede />
                <Footer />
            </Route>
            <Route path='/painel'>
                <Header />
                <Painel />
                <Footer />
            </Route>
            <Route path='/perfil'>
                <Header />
                <Perfil />
                <Footer />
            </Route>
            <Route path='/posts'>
                <Header />
                <Post />
                <Footer />
            </Route>
            <Route path='/signin'>
                <SignHeader />
                <SignIn />
                <SignFooter />
            </Route>
            <Route path='/signup'>
                <SignUp />
            </Route>
        </Switch>
    </Router>
    )
}