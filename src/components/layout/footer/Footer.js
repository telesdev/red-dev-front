import { Container, Row, Col } from 'react-bootstrap'
import styled from 'styled-components'
import { Col1 } from './Col1'
import { Col2 } from './Col2'
import { Col3 } from './Col3'

export const Footer = () => {
    return(
        <>
        <StyledContainer fluid>
            <Row>
                <Col sm={4}>
                    <Col1></Col1>
                </Col>

                <Col sm={4}>
                    <Col2></Col2>
                </Col>

                <Col sm={4}>
                    <Col3></Col3>
                </Col>
            </Row>
        </StyledContainer>
        </>
    ) 
}

const StyledContainer = styled(Container)`
    padding: 50px;
    background-color: rgb(30,30,30);
    color: white;
    text-align: center
`