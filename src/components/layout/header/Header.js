import { HeaderBreadcrumb } from '../breadcumb/HeaderBreadcrumb'
import { Navbar, Nav, Button, ButtonGroup, ButtonToolbar } from 'react-bootstrap'
import { Link } from 'react-router-dom';

export const Header = () => {
    
    return(
        <>
        <Navbar bg="danger" variant="dark" className='justify-content-between'>
            <Link to='/'>
                <Navbar.Brand>
                    <i className="fas fa-network-wired"></i>{' '}
                    Red-Dev Connection
                </Navbar.Brand>
            </Link>
            <Nav.Item>
                <ButtonToolbar aria-label="Toolbar with button groups">
                    <ButtonGroup>
                        <Link to='/posts'>
                            <Button variant='danger' className="mr-2">Postagens</Button>
                        </Link>
                        <Link to='/minharede'>
                            <Button variant='danger' className="mr-2">Minha Rede</Button>
                        </Link>
                        <Link to='/painel'>
                            <Button variant='danger' className="mr-2">Painel</Button>
                        </Link>
                        <Link to='/perfil'>
                            <Button variant='danger' className="mr-2">Perfil</Button>
                        </Link>
                    </ButtonGroup>
                </ButtonToolbar>
            </Nav.Item>
            <Nav.Item>
                <Button variant='danger'>Sair</Button>
            </Nav.Item>
            </Navbar>
        <HeaderBreadcrumb />
        </>
    ) 
}