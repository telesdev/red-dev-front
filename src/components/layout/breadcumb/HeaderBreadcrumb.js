import Breadcrumbs from 'react-router-dynamic-breadcrumbs-component';   

export const HeaderBreadcrumb = () => {
    const routes = {
        '/': 'Home',
        '/posts': 'Postagens', 
        '/minharede': 'Minha Rede',
        '/painel': 'Painel',
        '/perfil': 'Perfil'
    }

    return(
        <>
        <Breadcrumbs mappedRoutes={routes} />
        </>
    ) 
}
