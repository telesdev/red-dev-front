import { Navbar, Nav } from 'react-bootstrap'

export const SignHeader = () => {
    return(
        <>
        <Navbar bg="danger" variant="dark" className='justify-content-between'>
            <Navbar.Brand href="#home">
            <i class="fas fa-network-wired"></i>{' '}
            Red-Dev Connection
            </Navbar.Brand>
            <Nav>
                <Nav.Item>Ainda não tem cadastro?</Nav.Item>
                <Nav.Link href='/signup'>Clique aqui</Nav.Link>
                <Nav.Item>e cadastre-se agora!</Nav.Item>
            </Nav>
        </Navbar>
        </>
    ) 
}
