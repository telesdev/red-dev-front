import { Form, Container } from 'react-bootstrap'
import styled from 'styled-components'
import './signIn.css'

export const FormSignIn = () => {
    return (
        <>
        <StyledDiv>
            <StyledContainer>
                <Form>
                    <div>
                        <label htmlFor="email">E-mail:</label>
                        <input type="email" id="email" name="email" placeholder="Entre com o e-mail cadastrado" />
                    </div>
                    <div>
                        <label htmlFor="password">Senha:</label>
                        <input type="password" id="password" name="password" placeholder="Insira sua senha" />
                    </div>
                    <div>
                        <input type="checkbox" id="remember" name="remember" value="remember" />
                        <label htmlFor="remember"> Lembre-me</label>
                    </div>
                    <div>
                        <button type='submit'>Entrar</button>
                    </div>
                </Form>
            </StyledContainer>
        </StyledDiv>
        </>
    )
}

const StyledDiv = styled.div`
    width: 100%;
    height: 100vh;
    background: #EC6F66;
    background: -webkit-linear-gradient(to right, #F3A183, #EC6F66);
    background: linear-gradient(to right, #F3A183, #EC6F66);
`

const StyledContainer = styled(Container)`
    display: flex;
    align-content: center;
    text-align: center;
    justify-content: center;
    padding: 50% 0;

`