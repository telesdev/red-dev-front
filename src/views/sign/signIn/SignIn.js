import { Container, Col, Row } from 'react-bootstrap'
import { FormSignIn } from './FormSignIn'
import { LeftSideSignIn } from './LeftSideSignIn'

export const SignIn = () => {
    return (
        <>
        <Container fluid>
            <Row>
                <Col sm={8} className='m-0 p-0'>
                    <LeftSideSignIn />
                </Col>
                <Col sm={4} className='m-0 p-0'>
                    <FormSignIn />
                </Col>
            </Row>
        </Container>
        </>
    )
}