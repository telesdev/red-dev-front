import './signIn.css'
import backgroundImg from '../../../assets/imgs/background-signin.jpeg'
import styled from 'styled-components'

export const LeftSideSignIn = () => {
    return(
        <Div>
        </Div>
    ) 
}

const Div = styled.div`
    width: 100%;
    height: 100vh;
    float: left;
    background-image: url(${backgroundImg});
    background-size: 100% 100%;
    opacity: 0.4;
`